resource "aws_security_group" "test_public_sg" {
  name_prefix = "test-public-sg"
  description = "Security group for all public traffic"
  vpc_id      = "${aws_vpc.test-dev.id}"
  tags = {
    Name = "test_public_sg"
  }
}

resource "aws_security_group_rule" "test_public_sg_ingress_22" {
  description              = "Inbound https communication from main group"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.test_public_sg.id}"
  cidr_blocks 		       = "${var.my_network["my_cidr"]}"
  from_port                = 22
  to_port                  = 22
  type                     = "ingress"
}


#For allowing all outbound traffic
resource "aws_security_group_rule" "test_public_sg_egress" {
  description       = "Net egress"
  protocol          = "-1"
  security_group_id = "${aws_security_group.test_public_sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 0
  to_port           = 0
  type              = "egress"
}


resource "aws_security_group" "test_private_sg" {
  name_prefix = "test-sg"
  description = "Security group for all public traffic"
  vpc_id      = "${aws_vpc.test-dev.id}"
  tags = {
    Name = "test_private_sg"
  }
}

#For SSH access if needed
resource "aws_security_group_rule" "test_privingressfrompublic" {
  description              = "Inbound https communication from public instances"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.test_private_sg.id}"
  source_security_group_id = "${aws_security_group.test_public_sg.id}"
  from_port                = 0
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "test_privateingressfromprivate" {
  description              = "Inbound https communication from public instances"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.test_private_sg.id}"
  source_security_group_id = "${aws_security_group.test_private_sg.id}"
  from_port                = 0
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "test_private_sg_ingress_80" {
  description              = "Inbound https communication from outside"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.test_private_sg.id}"
  cidr_blocks              = ["0.0.0.0/0"]
  from_port                = 9200
  to_port                  = 9200
  type                     = "ingress"
}


#For allowing all outbound traffic
resource "aws_security_group_rule" "test_private_sg_egress" {
  description       = "Net egress"
  protocol          = "-1"
  security_group_id = "${aws_security_group.test_private_sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 0
  to_port           = 0
  type              = "egress"
}
