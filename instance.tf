data "aws_ami" "centos" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
#  owners   = ["679593333241"] # aws
  owners = ["099720109477"] # Canonical
}

resource "aws_key_pair" "key" {
  key_name   = "${var.key-pair["name"]}"
  public_key = "${var.key-pair["public_key"]}"
}

resource "aws_instance" "public-node" {
#  count         = "${length(data.aws_availability_zones.available_az.names)}"
  count         = 1
#  ami           = "${data.aws_ami.centos.id}"
  ami	= "ami-04db450be8ebc051c"
  instance_type = "${var.instance_type}"
  subnet_id     = "${element(aws_subnet.public_test.*.id, count.index)}"
#  key_name      = "${var.key-pair}"
  key_name      = "${aws_key_pair.key.id}"
  associate_public_ip_address = "true"
  vpc_security_group_ids = ["${aws_security_group.test_public_sg.id}"]
  tags = {
    Name  = "bastion"
    Owner = "test"
  }
}

resource "aws_instance" "private-node" {
#  count         = "${length(data.aws_availability_zones.available_az.names)}"
  count         = 1
#  ami           = "${data.aws_ami.centos.id}"
  ami   = "ami-04db450be8ebc051c"
  instance_type = "${var.es_instance_type}"
  #subnet_id    = "${aws_subnet.private_test.id}"
  subnet_id     = "${element(aws_subnet.private_test.*.id, count.index)}"
#  key_name      = "${var.key-pair}"
  key_name      = "${aws_key_pair.key.id}"
  user_data = "${file("install_es.sh")}"
  root_block_device {
    volume_size = 60
    volume_type = "gp2"
    delete_on_termination = true
  }
  vpc_security_group_ids = ["${aws_security_group.test_private_sg.id}"]
  tags = {
    Name  = "ES-${(count.index)}"
    Owner = "test"
  }
}

resource "aws_ebs_volume" "private-vol" {
#  count             = "${length(data.aws_availability_zones.available_az.names)}"
  count = 1
  availability_zone = "${element(data.aws_availability_zones.available_az.names, count.index)}"
  size              = 60
  tags = {
    Name = "Test"
    Owner = "test"
  }
}

resource "aws_volume_attachment" "private_ebs_attach" {
#  count = "${length(data.aws_availability_zones.available_az.names)}"
  count = 1
  device_name = "/dev/sdh"
  volume_id   = "${element(aws_ebs_volume.private-vol.*.id, count.index)}"
  instance_id = "${element(aws_instance.private-node.*.id, count.index)}"
}
