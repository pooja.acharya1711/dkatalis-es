#!/bin/bash
sudo apt-get update
sudo apt install -y apt-transport-https openjdk-8-jre-headless
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-6.x.list
sudo apt-get update && sudo apt-get install elasticsearch

cd /etc/elasticsearch
sudo mkdir -v certs

sudo /usr/share/elasticsearch/bin/elasticsearch-certutil ca --out certs/elastic-stack-ca.p12 --pass ""

sudo /usr/share/elasticsearch/bin/elasticsearch-certutil cert --ca certs/elastic-stack-ca.p12 --ca-pass "" --out certs/elastic-certificates.p12 --pass ""

sudo chmod +r certs/elastic-certificates.p12

echo -e "network.host: _site_\nxpack.security.enabled: true\nxpack.security.transport.ssl.enabled: true\nxpack.security.transport.ssl.verification_mode: certificate\nxpack.security.transport.ssl.keystore.path: certs/elastic-certificates.p12\nxpack.security.transport.ssl.truststore.path: certs/elastic-certificates.p12" |  sudo tee -a  /etc/elasticsearch/elasticsearch.yml

sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service

printf "Dkatalis" | sudo /usr/share/elasticsearch/bin/elasticsearch-keystore add "bootstrap.password" -x

sudo systemctl restart elasticsearch.service
