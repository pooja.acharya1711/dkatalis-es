resource "aws_lb" "test-nlb" {
  count		= 1
  name               = "test-network-lb"
  internal           = false
  load_balancer_type = "network"
   subnets            = ["${aws_subnet.public_test[count.index].id}"]
  
  enable_cross_zone_load_balancing = true
  tags = {
    Name = "test-lb"
    Owner = "test"
  }
}

resource "aws_lb_target_group" "test-tg-9200" {
  name     = "test-lb-tg-9200"
  port     = 9200
  protocol = "TCP"
  vpc_id   = "${aws_vpc.test-dev.id}"
}


resource "aws_lb_target_group_attachment" "lb-tg-9200" {
  count = "${length(data.aws_availability_zones.available_az.names)}"
  target_group_arn = "${aws_lb_target_group.test-tg-9200.arn}"
  target_id        = "${element(aws_instance.private-node.*.id, count.index)}"
  port             = 9200
}



resource "aws_lb_listener" "listener-9200" {
  count         = 1
  load_balancer_arn = "${aws_lb.test-nlb[count.index].arn}"
  port              = "9200"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.test-tg-9200.arn}"
  }
}
