#Insert your AWS credentials or attach IAM Role

variable "aws_access_key" {
  type    = "string"
  default = ""
}

variable "aws_secret_key" {
  type    = "string"
  default = ""
}

variable "region" {
  type    = "string"
  default = "ap-south-1"
}

variable "vpc" {
  type    = "map"
  default = {
    vpc_cidr = "10.0.0.0/16"
  }
}

#Insert your network IP to be whitelisted for SSH access on public machines

variable "my_network" {
  type    = "map"
  default = {
    my_cidr =  ["0.0.0.0/0"]
}
}

variable "key-pair" {
  type    = "map"
  default = {
    name = "test-key"
    public_key = ""
  }
}

variable "instance_type"{
  type    = "string"
  default = "t2.micro"
}

variable "es_instance_type"{
  type    = "string"
  default = "t2.medium"
}
